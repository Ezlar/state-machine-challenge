# State Machine Challenge Solution

![state_machine_graph_pdf](./state_machine_graph.pdf)

## Description

This project is aimed at solving the problem of mapping a state machine  by traversing it to discover nodes and edges.
In the interest of the creators of this challenge, the specifications of the problem are kept intentionally vague


## Setup

This project can be run in two ways.
Note, both require the creation of a .env file after cloning the repository.  
Consult the .env.example to see what information is required

### Local Setup

For Debian systems, you will need the following:

`sudo apt install graphviz`
`sudo apt install python3`
`sudo apt install pip3`

Once these packages are installed, we will need poetry in order to install the projecet's dependencies

`pip install poetry`

After cloning this repository, run the following form the root directory

`poetry install`

### Docker Setup

Docker and Docker compose are required
Linux instructions are available [here](https://docs.docker.com/engine/install/ubuntu/) and [here](https://docs.docker.com/compose/install/linux/#install-using-the-repository)

Once Docker has been setup, clone this repository and from the root folder run `docker-compose build`



## Running the project

The existence of a server that is serving the state machine is presumed, and the IP and Port for it will be required.

### Local
Run the following from the root directory of the project  
`poetry run python state_machine_challenge/main.py`  
Optionally, you can also pass a directory where the program will output a PDF of the graph representation of the state machine.  

`poetry run python state_machine_challenge/main.py path/to/folder`


### Docker

From the root directory of the project, execute the following:  
`docker-compose up state_machine_challenge`  
Note that if you wish to change the destination of the PDF output, you should edit [this mapping](https://gitlab.com/Ezlar/state-machine-challenge/-/blob/main/docker-compose.yml#L7) in the docker-compose.yml file to your desired destination

## Running tests

### Local
From the root directory, execute the following
`poetry run pytest`

### Docker
From the root directory of the project, execute the following:  
`docker-compose up state_machine_challenge_test`  

Test are also available via Gitlab CI/CD. You can find results [here](https://gitlab.com/Ezlar/state-machine-challenge/-/pipelines).  
Should you wish to run these in your own gitlab project, you will need to configure a variable in the Gitlab CI/CD settings called 'STATE_MACHINE_SERVER'.
This variable should of type "File" and the structure of it should match the .env file

## Notes on the solution

In order to generate the graph, afew different approaches are taken for traversal.  
Firstly, the majority of the graph is mapped by blind traversal between the states and utilizing any transitions that have not been yet explored.  
Secondly, a DFS approach is utilized to find and reach state that still have unexplored transitions.  
Finally, if the DFS fails to reach those states, a bi-directional dfs search is performed from the unifished state to the starting state

Per the specification of this problem, it seems to be indicated that there is no transition label for the transition between the final state and the initial state.  
As such, liberty was taken in marking the transition as '0', though this could be easily altered if required.

It should be noted that for most state machine configurations, this solution is able to compeltely map all nodes and states.
However, in some state machine configurations, the program is likely to incur very deep recursion and though it should eventually completed the graph, it may take quite a bit of time. In those instanced, this process may also become very resource intensive and thus it may be easier to quit and try it again against a different state machine structure.  
This flaw could be ameneded witht the implementation of a data structure to record the shortest paths between states, in order to make traversal simpler when required






