FROM python:alpine3.17

RUN apk add graphviz
RUN apk add ttf-freefont

WORKDIR /state_machine_challenge

COPY . /state_machine_challenge/

RUN pip3 install poetry

RUN poetry install