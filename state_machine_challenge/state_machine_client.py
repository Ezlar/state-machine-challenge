"""
Module for connecting to the state machine server
"""

import socket


class StateMachineClient:
    """
    Class for managing connection to the state machine server
    """

    def __init__(self, host: str, port: int) -> None:

        self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection.connect((host,port))

    def __del__(self):
        self.connection.close()

    def receive(self) -> bytes:
        """
        Retrieve message from the server
        """

        return self.connection.recv(1024).decode().strip()

    def send(self, message: str) -> None:
        """
        Send messages to the server. <LF> padding automatically added.
        """

        return self.connection.sendall((message + "\n").encode())
