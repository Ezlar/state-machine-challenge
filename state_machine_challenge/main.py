"""
Main module for the program
"""

import os
import sys
from argparse import ArgumentParser
from pathlib import Path
from collections import deque
from dotenv import load_dotenv
from state_machine_challenge.state_machine_client import StateMachineClient
from state_machine_challenge.state_machine_graph import StateMachineGraph

def main(output_path: str):
    """
    Main Function
    """
    load_dotenv()
    address = os.getenv('SERVER_IP')
    port = int(os.getenv('PORT'))
    max_nodes = int(os.getenv('NODE_NUMBER'))
    max_edges = int(os.getenv('EDGE_NUMBER'))
    connection = StateMachineClient(address, port)
    graph = StateMachineGraph(path=output_path)
    current_node = connection.receive()
    remaining_transitions = deque([])

    print("Starting graph traversal")
    while len(graph.nodes) < max_nodes or graph.get_total_edge_count() < max_edges:
        if current_node == 'Z':
            graph.add_node(current_node)
            graph.add_edge(current_node, 'A', 0)
            current_node = connection.receive()
        graph.add_node(current_node)

        if not remaining_transitions:
            if tranisitions := graph.check_remaining_transition(current_node):
                remaining_transitions.append(tranisitions.pop())
            else:
                if path_to_unfinished := graph.depth_first_search(current_node, [], []):
                    remaining_transitions.extend(path_to_unfinished)
                else:
                    remaining_transitions.extend(graph.targeted_search(current_node))

        transition = remaining_transitions.popleft()
        connection.send(str(transition))
        new_node = connection.receive()
        graph.add_edge(current_node, new_node, transition)
        current_node = new_node

    print(f"Found {len(graph.nodes)} nodes and {graph.get_total_edge_count()} edges")
    graph.smg.render()


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument(
        "output_path",
        nargs='?',
        help="Optional argument for the destination of the graph pdf file"
    )
    args = parser.parse_args()
    path = args.output_path if args.output_path else ''
    directory = Path(path)
    if not directory.is_dir():
        sys.exit(f"{path} is not a valid directory")
    main(path)
