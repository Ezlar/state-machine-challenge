"""
Wrapper class for graphviz object to add some helper functions
"""

from graphviz import Digraph

class Edge:
    """
    Class to store edge information
    """

    def __init__(self, start: str, end: str, transition: int) -> None:
        self.start = start
        self.end = end
        self.transition = transition

    def __eq__(self, other):
        if isinstance(other, Edge):
            return (
                self.start == other.start and
                self.end == other.end and
                self.transition == other.transition
            )
        return False

    def __key(self):
        return (self.start, self.end, self.transition)

    def __hash__(self):
        return hash(self.__key())

    def __repr__(self):
        return f"({self.start}, {self.end}, {self.transition})"

    def __str__(self):
        return f"({self.start}, {self.end}, {self.transition})"

class StateMachineGraph:
    """
    Define some methods to help handle the graphviz diagram
    """

    def __init__(self, name: str = "state_machine_graph", path = '') -> None:
        """
        Initialize an empty graphviz Digraph object
        """

        node_attr = {
            "color": "#577fb3",
            "shape": "circle",
            "style": "filled",
            "fillcolor": "#eeeeee"
        }
        edge_attr = {"color": "#577fb3"}
        self.smg = Digraph(
            name,
            filename=name,
            format="pdf",
            node_attr=node_attr,
            edge_attr=edge_attr,
            directory=path

        )
        self.nodes = []
        self.edges = {}

    def add_node(self, name: str) -> bool:
        """
        Check if node already exists before adding
        """

        if name not in self.nodes:
            self.smg.node(name)
            self.nodes.append(name)
            return True

        return False

    def add_edge(self, start: str, end: str, transition: int) -> None:
        """
        Keep an internal representation of edges
        """

        if start not in self.edges:
            self.edges[start] = []

        edge = Edge(start, end, transition)
        if edge not in self.edges[start]:
            if transition not in self.get_transition_list(start):
                self.edges[start].append(edge)
                self.smg.edge(start, end, label=str(transition))

    def check_edge(self, node: str) -> list | None:
        """
        Check to see what edges have already been found for given node
        """

        if node in self.edges:
            return self.edges[node]

        return None

    def get_edge(self, start: str, end: str) -> Edge | None:
        """
        Find the edge between two nodes. Return none if there is no edge
        """

        match = [match for match in self.edges[start] if match.end == end]
        return match.pop() if match else None

    def get_total_edge_count(self):
        """
        Calculate how many edges have been discovered
        """

        return sum(len(edgelist) for node, edgelist in self.edges.items())


    def get_transition_list(self, node: str) -> list:
        """
        Iterate over all discovered edges for a node, and
        return a list of transitions that have already been
        explored
        """

        transitions = []
        if node not in self.edges:
            return transitions
        for edge in self.edges[node]:
            transitions.append(edge.transition)
        return transitions

    def check_remaining_transition(self, node: str) -> list | None:
        """
        Looks through a node's edges to see if there are any
        unexplored transitions (1,2,3). Z is a special case, as
        it is the only node that doesn't have three transitions
        """

        if node not in self.nodes:
            return None
        if node == 'Z':
            return []
        transitions = self.get_transition_list(node)
        return list(set([1, 2, 3]) - set(transitions))

    def get_unfinished_node(self):
        """
        Find and return a node that still has
        remaining undiscovered transitions
        """

        for node, edges in self.edges.items():
            if node != 'Z':
                if len(edges) < 3:
                    return node
        return None

    def depth_first_search(self, node: str, instructions: list, visited: list) -> list:
        """
        Check neighbouring nodes for remaining traversals
        """

        if self.check_remaining_transition(node):
            return instructions

        for edge in self.edges[node]:
            if edge not in visited:
                new_visited = visited.copy()
                new_visited.append(edge)
                new_instructions = instructions.copy()
                if edge.start != 'Z':
                    new_instructions.append(edge.transition)
                result = self.depth_first_search(edge.end, new_instructions, new_visited)
                if result:
                    return result

        return None

    def backwards_search(self, dest_node: str, instructions: list, visited: list):
        """
        Search backwards from a node to try and find a path to A
        """

        if dest_node == 'A':
            return instructions

        for node, edges in self.edges.items():
            for edge in edges:
                if edge.end == dest_node and edge not in visited:
                    new_instructions = instructions.copy()
                    new_instructions.insert(0,edge.transition)
                    new_visited = visited.copy()
                    new_visited.append(edge)
                    return self.backwards_search(node, new_instructions, new_visited)

        return None

    def path_to_start(self, node: str, instructions: list, visited: list):
        """
        Search a path to the start node from the current node
        """

        if node == 'A':
            return instructions

        for edge in self.edges[node]:
            if edge not in visited:
                if edge.start != 'Z':
                    new_instructions = instructions.copy()
                    new_instructions.append(edge.transition)
                    new_visited = visited.copy()
                    new_visited.append(edge)
                result = self.path_to_start(edge.end, new_instructions, new_visited)
                if result:
                    return result

        return None

    def targeted_search(self, node: str):
        """
        Try an abridged bi-directional search when dfs isn't succeeding
        """

        remaining_node = self.get_unfinished_node()
        path_to_start = self.path_to_start(node, [], [])
        path_from_remaining_node = self.backwards_search(remaining_node, [], [])
        return path_to_start.extend(path_from_remaining_node)
