"""
Tests for the State Machine Graph class
"""

from state_machine_challenge.state_machine_graph import StateMachineGraph, Edge

class TestStateMachineGraph:
    """
    Test suite for the State Machine Graph class
    """

    def test_add_node(self):
        """
        Adding a node should update the internal list
        as well as the graphviz object
        """
        graph = StateMachineGraph()
        graph.add_node('A')

        assert 'A' in graph.nodes
        assert '\tA\n' in graph.smg.body


    def test_cant_add_node_twice(self):
        """
        The list of nodes should have no duplicates
        """
        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_node('A')

        assert graph.nodes.count('A') == 1
        assert graph.smg.body.count('\tA\n') == 1

    def test_add_edge(self):
        """
        If a node has been added, we should be able to
        define an edge for it
        """
        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'B', 2)

        edge = Edge('A', 'B', 2)
        assert edge in graph.edges['A']
        assert '\tA -> B [label=2]\n' in graph.smg.body


    def test_cant_add_edge_twice(self):
        """
        We should not be able to add duplicate edges
        """
        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'B', 2)
        graph.add_edge('A', 'B', 2)

        edge = Edge('A', 'B', 2)
        assert graph.edges['A'].count(edge) == 1
        assert graph.smg.body.count('\tA -> B [label=2]\n') == 1

    def test_cant_add_edge_same_transition(self):
        """
        We should not be able to add two edges for a node that have
        the same transition value, since each node should have exactly
        three transitions (1,2,3) and no duplicate transitions
        """
        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'B', 2)
        graph.add_edge('A', 'D', 2)

        edge = Edge('A', 'B', 2)
        invalid_edge = Edge('A', 'D', 2)
        assert edge in graph.edges['A']
        assert '\tA -> B [label=2]\n' in graph.smg.body
        assert invalid_edge not in graph.edges['A']
        assert '\tA -> D [label=2]\n' not in graph.smg.body

    def test_check_edge(self):
        """
        We should be able to receive a list
        of known edges for a node
        """

        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'B', 2)
        graph.add_edge('A', 'C', 1)

        first_edge = Edge('A', 'B', 2)
        second_edge = Edge('A', 'C', 1)

        edges = graph.check_edge('A')
        assert first_edge in edges
        assert second_edge in edges

    def test_get_transition_list(self):
        """
        Get a list of all the paths taken from a node
        """

        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'C', 1)
        graph.add_edge('A', 'B', 2)

        assert graph.get_transition_list('A') == [1,2]

    def test_check_remaining_transition(self):
        """
        Check what transitions have not been taken yet from a node
        """

        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'C', 1)
        graph.add_edge('A', 'B', 2)

        assert graph.check_remaining_transition('A') == [3]

    def test_check_remainig_transitions_no_edges(self):
        """
        If we have never left a node, there are no edges
        defined for it and we should receive a list of all
        possible transitions
        """

        graph = StateMachineGraph()
        graph.add_node('A')
        assert graph.check_remaining_transition('A') == [1,2,3]

    def test_check_remainig_transitions_all_edges(self):
        """
        If all transitions have been taken, we should recive an
        empty list
        """

        graph = StateMachineGraph()
        graph.add_node('A')
        graph.add_edge('A', 'C', 1)
        graph.add_edge('A', 'B', 2)
        graph.add_edge('A', 'D', 3)
        assert graph.check_remaining_transition('A') == []

    def test_check_remaining_transition_no_node(self):
        """
        If we haven't visited a node, we cannot return any
        remaining transitions, and instead we should receive None
        """

        graph = StateMachineGraph()
        assert graph.check_remaining_transition('A') is None

    def test_get_edge(self):
        """
        Check that we are able to retrieve an edge from the graph
        """

        graph = StateMachineGraph()
        graph.add_edge('A', 'C', 1)

        edge = Edge('A', 'C', 1)
        assert edge == graph.get_edge('A', 'C')

    def test_get_edge_returns_none(self):
        """
        Check that we return nothing if we can't find the edge
        """

        graph = StateMachineGraph()
        graph.add_edge('A', 'C', 1)


        assert graph.get_edge('A', 'B') is None

    def test_get_total_edge_count(self):
        """
        Check we get an accurate readout of the number of found edges
        """

        graph = StateMachineGraph()
        graph.add_edge('A', 'C', 1)
        graph.add_edge('A', 'B', 2)
        graph.add_edge('A', 'D', 3)
        graph.add_edge('F', 'G', 1)

        assert graph.get_total_edge_count() == 4

    def test_get_unfinished_node(self):
        """
        Check that we are abled to find nodes that have edges
        still remaining to be explored
        """

        graph = StateMachineGraph()
        graph.add_edge('A', 'C', 1)
        graph.add_edge('A', 'B', 2)
        graph.add_edge('B', 'B', 2)
        graph.add_edge('B', 'C', 1)
        graph.add_edge('B', 'D', 3)

        assert graph.get_unfinished_node() == 'A'


    def test_get_unfinished_node_if_all_explored(self):
        """
        Check that if all nodes have had all edges explored
        we return None
        """

        graph = StateMachineGraph()
        graph.add_edge('A', 'C', 1)
        graph.add_edge('A', 'B', 2)
        graph.add_edge('B', 'B', 2)
        graph.add_edge('B', 'C', 1)
        graph.add_edge('B', 'D', 3)

        assert graph.get_unfinished_node() == 'A'
        graph.add_edge('A', 'F', 3)
        assert graph.get_unfinished_node() is None
