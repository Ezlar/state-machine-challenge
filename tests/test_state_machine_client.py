"""
Tests for the State Machine Client class
"""

import os
import random
import pytest
from state_machine_challenge.state_machine_client import StateMachineClient


class TestStateMachineClient:
    """
    Test the receive and send methods of the class
    """

    @pytest.fixture(scope="class")
    def client(self):
        """
        Construct the client object for the tests
        """
        address = os.getenv('SERVER_IP')
        port = int(os.getenv('PORT'))
        return StateMachineClient(address, port)


    def test_receive(self, client):
        """
        The server should always respond with state A after connection is established
        """

        assert client.receive() == 'A'

    def test_send(self, client):
        """
        The structure of the state machine is randomised when the conneciton is open,
        but any valid instruction [1,3] should return a node in the range (A,Z]
        """

        instruction = random.randint(1,3)
        client.send(str(instruction))
        reply = client.receive()
        assert reply.isalpha()
        assert len(reply) == 1
        assert reply != 'A'
